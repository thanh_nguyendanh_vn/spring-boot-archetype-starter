package ${package}.${artifactId}.aop;

import ${package}.${artifactId}.logger.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class RepositoryAspect {

    private Logger logger = Logger.getInstance(this.getClass());

    @Around("execution(* ${package}.${artifactId}.repository.*.*(..))")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;
        String message = joinPoint.getSignature() + " exec in " + executionTime + " ms";
        if (executionTime >= 1000){
            logger.warn(message + " ---> SLOW QUERY");
        }
        return proceed;
    }
}
