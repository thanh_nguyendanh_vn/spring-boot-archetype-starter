package ${package}.${artifactId}.exception;

import ${package}.${artifactId}.constant.ResponseStatusCodeEnum;
import ${package}.${artifactId}.factory.GeneralResponse;
import ${package}.${artifactId}.factory.ResponseStatus;
import ${package}.${artifactId}.logger.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private Logger log = Logger.getInstance(this.getClass());

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<?> handleAllExceptions(Exception ex) {
        return this.createResponse(ResponseStatusCodeEnum.INTERNAL_GENERAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({BusinessException.class})
    public final ResponseEntity<?> handleValidationExceptions(RuntimeException ex) {
        return this.createResponse(ResponseStatusCodeEnum.BUSINESS_ERROR, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<?> createResponse(ResponseStatusCodeEnum response, HttpStatus status) {
        ResponseStatus responseStatus = new ResponseStatus(response.getCode(), response.getMessage());
        GeneralResponse<Object> responseObject = new GeneralResponse<>();
        responseObject.setStatus(responseStatus);
        return new ResponseEntity<>(responseObject, status);
    }
}
