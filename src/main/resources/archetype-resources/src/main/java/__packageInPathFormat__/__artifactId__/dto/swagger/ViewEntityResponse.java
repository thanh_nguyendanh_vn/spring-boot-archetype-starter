package ${package}.${artifactId}.dto.swagger;

import ${package}.${artifactId}.dto.response.ExampleEntityResponseDto;
import ${package}.${artifactId}.factory.GeneralResponse;

// use this class as a wrapper in the swagger doc in case you need to
public class ViewEntityResponse extends GeneralResponse<ExampleEntityResponseDto> {}
