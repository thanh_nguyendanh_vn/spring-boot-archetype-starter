package ${package}.${artifactId}.config;

import java.util.concurrent.TimeUnit;

import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import javax.cache.spi.CachingProvider;

import org.ehcache.jsr107.EhcacheCachingProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Register your local caching here.
 */
@Configuration
@EnableCaching
public class LocalCachingConfig {
	@Value("${caching.time-to-live.ms}")
	private Long ttlMs;
	
	@Bean
	public CacheManager configureEhCache() {
		CachingProvider cachingProvider = Caching.getCachingProvider();
		EhcacheCachingProvider ehcacheProvider = (EhcacheCachingProvider) cachingProvider; 
		MutableConfiguration<?, ?> configuration = new MutableConfiguration<>().setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new Duration(TimeUnit.MILLISECONDS, ttlMs)));
		CacheManager cacheManager = ehcacheProvider.getCacheManager();
		// register your caches here
         cacheManager.createCache("method1ToBeCached", configuration);
         cacheManager.createCache("method2ToBeCached", configuration);
		return cacheManager;
	}
}
