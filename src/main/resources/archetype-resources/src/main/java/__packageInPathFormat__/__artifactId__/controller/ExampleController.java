package ${package}.${artifactId}.controller;

import ${package}.${artifactId}.dto.request.ExampleEntityRequestDto;
import ${package}.${artifactId}.exception.BusinessException;
import ${package}.${artifactId}.factory.GeneralResponse;
import ${package}.${artifactId}.factory.ResponseFactory;
import ${package}.${artifactId}.service.ExampleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/example/entity")
public class ExampleController {
    @Autowired
    private ResponseFactory responseFactory;

    @Autowired
    private ExampleService exampleService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create a example entity", response = GeneralResponse.class)
    public ResponseEntity<?> createExampleEntity(@Valid @RequestBody ExampleEntityRequestDto request,
                                                 @ApiIgnore Errors errors) {
        if (errors != null && errors.hasErrors()) {
            throw new BusinessException("Invalid Parameter");
        }
        exampleService.saveEntity(request);
        return responseFactory.success();
    }
}
