package ${package}.${artifactId}.config;

import ${package}.${artifactId}.logger.Logger;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class AppConfig {
    private Logger logger = Logger.getInstance(this.getClass());

    @Value("${exampleConfiguration}")
    String exampleConfiguration;

    @Value("${application-short-name}")
    String applicationShortName;

}
