package ${package}.${artifactId}.service;

import ${package}.${artifactId}.dto.request.ExampleEntityRequestDto;
import ${package}.${artifactId}.entity.ExampleEntity;
import ${package}.${artifactId}.mapper.ExampleEntityMapper;
import ${package}.${artifactId}.repository.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExampleService {

    @Autowired
    private ExampleRepository exampleRepository;

	public ExampleEntity saveEntity(ExampleEntityRequestDto requestDto) {
	    ExampleEntity exampleEntity = ExampleEntityMapper.INSTANCE.requestDtoToEntity(requestDto);
	    exampleRepository.save(exampleEntity);
	    return exampleEntity;
	}
}
