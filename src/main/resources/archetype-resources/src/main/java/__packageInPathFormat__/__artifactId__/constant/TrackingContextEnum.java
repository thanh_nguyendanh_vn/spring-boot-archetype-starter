package ${package}.${artifactId}.constant;

import lombok.Getter;
import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum TrackingContextEnum {

    X_FORWARD_FOR("x-forwarded-for", "clientIP"),
    X_CORRELATION_ID("X-Correlation-ID", "correlationID");

    private final String headerKey;
    private final String threadKey;
}
