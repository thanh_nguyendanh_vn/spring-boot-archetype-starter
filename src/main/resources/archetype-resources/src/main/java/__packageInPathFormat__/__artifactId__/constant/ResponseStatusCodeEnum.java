package ${package}.${artifactId}.constant;

import lombok.*;

@Getter
@AllArgsConstructor
public enum ResponseStatusCodeEnum {
    SUCCESS("0", "Success"),
    BUSINESS_ERROR("ES0001", "Business error"),
    VALIDATION_ERROR("ES0002", "Validation error"),
    INTERNAL_GENERAL_SERVER_ERROR("ES0003", "General error");
    // Adds more response codes here

    private String code;
    private String message;

    @Override
    public String toString() {
        return "ResponseStatus{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
