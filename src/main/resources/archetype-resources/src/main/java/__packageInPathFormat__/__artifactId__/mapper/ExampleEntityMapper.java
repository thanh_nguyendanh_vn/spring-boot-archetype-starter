package ${package}.${artifactId}.mapper;

import ${package}.${artifactId}.dto.request.ExampleEntityRequestDto;
import ${package}.${artifactId}.dto.response.ExampleEntityResponseDto;
import ${package}.${artifactId}.entity.ExampleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExampleEntityMapper {
    ExampleEntityMapper INSTANCE = Mappers.getMapper(ExampleEntityMapper.class);
	ExampleEntity requestDtoToEntity(ExampleEntityRequestDto requestDto);
	ExampleEntityResponseDto entityToResponseDto(ExampleEntity entity);
}
