package ${package}.${artifactId}.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 7477925234189144413L;
    public BusinessException(String message) {
        super(message);
    }
}
