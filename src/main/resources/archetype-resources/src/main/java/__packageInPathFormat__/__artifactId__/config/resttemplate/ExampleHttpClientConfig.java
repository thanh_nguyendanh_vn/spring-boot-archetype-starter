package ${package}.${artifactId}.config.resttemplate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Additional configurations could be grouped into object-like architecture for better management and access
 */
@Component
@ConfigurationProperties(prefix="example-rest-config")
@Getter
@Setter
@ToString
public class ExampleHttpClientConfig {
    private String endpoint;
}
