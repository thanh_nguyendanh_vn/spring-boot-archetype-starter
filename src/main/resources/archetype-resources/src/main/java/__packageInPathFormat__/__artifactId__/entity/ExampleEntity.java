package ${package}.${artifactId}.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "example_entity")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ExampleEntity {
	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "example_field")
	private String exampleField;

    @Column(name = "created_date")
    private String createdDate;
}
