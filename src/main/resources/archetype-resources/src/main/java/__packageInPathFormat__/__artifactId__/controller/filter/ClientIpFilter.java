package ${package}.${artifactId}.controller.filter;

import ${package}.${artifactId}.constant.TrackingContextEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@Order(2)
public class ClientIpFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        String ipList = StringUtils.defaultIfEmpty(httpServletRequest.getHeader(TrackingContextEnum.X_FORWARD_FOR.getHeaderKey()), httpServletRequest.getRemoteAddr());
        ThreadContext.put(TrackingContextEnum.X_FORWARD_FOR.getThreadKey(), ipList);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        ThreadContext.clearAll();
    }
}
