package ${package}.${artifactId}.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.util.Date;

@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors
                        .basePackage("${package}.${artifactId}.controller"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(apiEndPointsInfo())
                .directModelSubstitute(Timestamp.class, Date.class);
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("An example service")
                .description("An example description")
                .contact(new Contact("Fullstack team", "https://www.ascendcorp.com/", "example@g.ascendcorp.com"))
                .license("Ascends Tech - Fullstack")
                .licenseUrl("https://www.ascendcorp.com/")
                .version("1.0.0")
                .build();
    }
}
