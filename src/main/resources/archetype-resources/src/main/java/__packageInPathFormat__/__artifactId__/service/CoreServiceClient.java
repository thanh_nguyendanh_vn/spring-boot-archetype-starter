package ${package}.${artifactId}.service;

import ${package}.${artifactId}.config.resttemplate.ExampleHttpClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class CoreServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ExampleHttpClientConfig exampleHttpClientConfig;

    public void exampleCallToClient() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(exampleHttpClientConfig.getEndpoint());
        HttpHeaders httpHeaders = new HttpHeaders();
        // add new headers here
        HttpEntity requestEntity = new HttpEntity<>(httpHeaders);
        restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, requestEntity,
                Object.class).getBody();
    }
}
