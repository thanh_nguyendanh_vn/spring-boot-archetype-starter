package ${package}.${artifactId}.controller.filter;

import ${package}.${artifactId}.config.AppConfig;
import ${package}.${artifactId}.constant.TrackingContextEnum;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Configuration
@Order(1)
public class LogCorrelationFilter extends OncePerRequestFilter {

    @Autowired
    private AppConfig appConfig;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        generateCorrelationIdIfNotExists(request.getHeader(TrackingContextEnum.X_CORRELATION_ID.getHeaderKey()));
        response.setHeader(TrackingContextEnum.X_CORRELATION_ID.getHeaderKey(), ThreadContext.get(TrackingContextEnum.X_CORRELATION_ID.getThreadKey()));
        chain.doFilter(request, response);
        ThreadContext.clearAll();
    }

    private void generateCorrelationIdIfNotExists(String xCorrelationId) {
        String correlationId = StringUtils.isEmpty(xCorrelationId) ? String.format(appConfig.getApplicationShortName() + "-%s", UUID.randomUUID().toString().replace("-", "").toLowerCase()).trim() : xCorrelationId;
        ThreadContext.put(TrackingContextEnum.X_CORRELATION_ID.getThreadKey(), correlationId);
    }
}
