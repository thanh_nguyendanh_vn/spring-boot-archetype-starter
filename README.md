 Spring Boot 2 Quickstart Maven Archetype
=========================================
Summary
-------
The project is a Maven archetype for Spring Boot web application 
which has all common standards on place ready for development

- Java 1.8+
- Maven 3.5+
- Spring boot 2.1.0.RELEASE+
- Lombok abstraction
- JPA with H2 for explanation
- Swagger 2 API documentation
- Support retry in sanity checks 
- Logback configuration  
- Logbook logging
- AOP filter queries  


Installation
------------

To install the archetype in your local repository execute following commands:

```sh
$ git clone https://bitbucket.org/thanh_nguyendanh_vn/spring-boot-archetype-starter/src/master/
$ cd spring-boot-archetype-starter
$ mvn clean install
```

Create a project
----------------

```sh
$ mvn archetype:generate \
     -DarchetypeGroupId=com.acm.spring-boot-archetypes \
     -DarchetypeArtifactId=spring-boot-quickstart \
     -DarchetypeVersion=1.0.0 \
     -DgroupId=com.test \
     -DartifactId=sampleapp \
     -Dversion=1.0.0-SNAPSHOT \
     -DinteractiveMode=false
```

Test on the browser via SWAGGER
-------------------

```sh
http://localhost:8080/swagger-ui.html
```

Directory breakdown
-------------------

```
    src
    └── main
        ├── java
        │   └── __packageInPathFormat__
        │       └── __artifactId__
        │           ├── aop
        │           ├── config
        │           │   └── resttemplate
        │           ├── constant
        │           ├── controller
        │           │   └── filter
        │           ├── dto
        │           │   ├── request
        │           │   ├── response
        │           │   └── swagger
        │           ├── entity
        │           ├── exception
        │           ├── factory
        │           ├── logger
        │           ├── mapper
        │           ├── repository
        │           ├── service
        │           └── util
        └── resources
```

- ```aop``` : package for Aspected Oriented Programming filters. 
- ```config``` : package for application-specific configuration. 
- ```constant``` : package for declaring constants, enums i.e. response codes.
- ```controller``` : package for declaring controllers of the application. 
- ```dto``` : data transfer objects, which are objects used to communicate with other services.
    - ```dto.request``` : requests from other services
    - ```dto.response``` : responses from other services
    - ```dto.swagger``` : wrapper response objects for swagger annotation. 
- ```entity``` : Entities from database. 
- ```exception``` : package for declaring exceptions
- ```factory``` : package for declaring factories
- ```logger``` : package for logging related aspect i.e. custom logger
- ```mapper``` : package for mapper between dto and entities
- ```repository```: package for database-related repositories
- ```service``` : package for business-related services. 
- ```util``` : package for any helper, auxiliary methods